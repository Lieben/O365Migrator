Param([Hashtable]$communicatieObject)
$communicatieObject.Interface = @{}

#Load WPF Framework (.NET)
Add-Type -AssemblyName PresentationCore,PresentationFramework,WindowsBase,system.windows.forms

[xml]$xaml = @"
    <Window
            xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
            Title="O365Migrator v1.00 by Lieben.nu" Height="451.117" Width="847.153" ResizeMode="CanMinimize" Topmost="True" WindowStartupLocation="CenterScreen">
        <Grid Margin="0,0,4,0">
            <Label Name="label" Content="O365Migrator v1.00 by Lieben.nu" HorizontalAlignment="Left" Margin="150,0,0,0" VerticalAlignment="Top" Height="41" Width="476" FontSize="21.333"/>
            <TextBox Name="iLog" ScrollViewer.HorizontalScrollBarVisibility="Disabled" ScrollViewer.VerticalScrollBarVisibility="Auto" HorizontalAlignment="Left" Height="124" Margin="10,288,0,0" TextWrapping="Wrap" Text="$(Get-Date): Hi there! I'll keep you posted on what is going on" VerticalAlignment="Top" Width="817"/>
            <Label Name="label1" Content="O365 Username:" HorizontalAlignment="Left" Margin="10,58,0,0" VerticalAlignment="Top"/>
            <Label Name="label1_Copy" Content="O365 Password:" HorizontalAlignment="Left" Margin="10,89,0,0" VerticalAlignment="Top"/>
            <ProgressBar Name="iBar" HorizontalAlignment="Left" Height="18" Margin="10,265,0,0" VerticalAlignment="Top" Width="817"/>
            <TextBox Name="iLoginBox" HorizontalAlignment="Left" Height="23" Margin="122,61,0,0" TextWrapping="Wrap" Text="" VerticalAlignment="Top" Width="216"/>
            <PasswordBox Name="iPasswordBox" HorizontalAlignment="Left" Margin="122,92,0,0" VerticalAlignment="Top" Width="216" Height="23"/>
            <Button Name="iConnect" Content="Connect" HorizontalAlignment="Left" Margin="10,129,0,0" VerticalAlignment="Top" Width="75" Height="27"/>
            <Label Name="iUploadedStatus" Content="" HorizontalAlignment="Left" Margin="10,230,0,0" VerticalAlignment="Top" Height="30" Width="340"/>
            <Label Name="iConnectResult" Content="" HorizontalAlignment="Left" Margin="90,130,0,0" VerticalAlignment="Top" Height="30" Width="198"/>
            <TabControl Name="tabControl" HorizontalAlignment="Left" Height="202" Margin="353,58,0,0" VerticalAlignment="Top" Width="474">
                <TabItem Header="Provisioning">
                    <Grid>
                        <Label Name="label3" Content="Before uploading data to Onedrive for Business, user storage &#xA;space has to be allocated (pre-provisioned). If you have not yet &#xA;done so, click the button and it will magically happen.&#xA;" HorizontalAlignment="Left" Margin="10,10,0,0" VerticalAlignment="Top" Height="71" Width="355"/>
                        <Button Name="iProvision" Content="Provision user storage" HorizontalAlignment="Left"  VerticalAlignment="Top" Width="135" Height="27" Margin="10,81,0,0"/>
                        <Label Name="iProvisionResult" Content="" HorizontalAlignment="Left" VerticalAlignment="Top" Height="30" Width="78" Margin="150,78,0,0"/>
                    </Grid>
                </TabItem>
                <TabItem Header="Authorization">
                    <Grid>
                        <Label Name="label4" Content="Your account needs permission on each user's Onedrive storage&#xA;space to upload data. Click the button to do automatically&#xA;do so.&#xA;" HorizontalAlignment="Left" Margin="10,10,0,0" VerticalAlignment="Top" Height="71" Width="355"/>
                        <Button Name="iAuthorize" Content="Set permissions" HorizontalAlignment="Left"  VerticalAlignment="Top" Width="117" Height="27" Margin="10,81,0,0"/>
                        <Button Name="iunAuthorize" Content="Remove permissions" HorizontalAlignment="Left"  VerticalAlignment="Top" Width="117" Height="27" Margin="10,128,0,0"/>
                        <Label Name="iAuthorizeResult" Content="" HorizontalAlignment="Left" VerticalAlignment="Top" Height="30" Width="78" Margin="132,78,0,0"/>
                    </Grid>
                </TabItem>
                <TabItem Header="Homedir Migration">
                    <Grid>
                        <Label Name="label5" Content="Navigate to the CSV file with your user's login names and paths&#xD;&#xA;to their homedirectories. An example file was supplied with this&#xD;&#xA;script. " HorizontalAlignment="Left" Margin="10,10,0,0" VerticalAlignment="Top" Height="57" Width="448" Grid.ColumnSpan="4"/>
                        <TextBox Name="iCSVPath" HorizontalAlignment="Left" Height="23" TextWrapping="Wrap" Text="" VerticalAlignment="Top" Width="342" Margin="10,67,0,0"/>
                        <Button Name="iBrowseCSV" Content="Browse for CSV" HorizontalAlignment="Left" Margin="364,67,0,0" VerticalAlignment="Top" Width="96" Height="23"/>
                        <Button Name="iVerifyCSV" Content="Scan &amp; Fix" HorizontalAlignment="Left" Margin="10,141,0,0" VerticalAlignment="Top" Width="106" Height="23" ToolTip="Scan will first correct all folder and file names to comply with Onedrive for Business standards."/>
                        <Button Name="iVerifyCSVNoCommit" Content="Scan Only" HorizontalAlignment="Left" Margin="166,141,0,0" VerticalAlignment="Top" Width="106" Height="23" ToolTip="Scan Only will produce a log of potential issues that can be corrected if you run in scan and fix mode."/>
                        <Button Name="iUploadUser" Content="Upload" HorizontalAlignment="Left" Margin="322,141,0,0" VerticalAlignment="Top" Width="96" Height="23"/>
                        <TextBox Name="iHDSubFolder" HorizontalAlignment="Left" Height="23" TextWrapping="Wrap" Text="" VerticalAlignment="Top" Width="342" Margin="10,95,0,0" ToolTip="If specified, this folder will be created and used, instead of the root of the user's onedrive"/>
                        <Label Name="iSfN1" Content="Subfolder name" HorizontalAlignment="Left" Margin="364,92,0,0" VerticalAlignment="Top" ToolTip="If specified, this folder will be created and used, instead of the root of the user's onedrive"/>
                    </Grid>
                </TabItem>
                <TabItem Header="Folder Migration">
                    <Grid>
                        <TextBox Name="iSpURL" HorizontalAlignment="Left" Height="23" TextWrapping="Wrap" Text="" VerticalAlignment="Top" Width="347" Margin="10,6,0,0" ToolTip="The URL to your sharepoint site, for example: https://ogd.sharepoint.com/site1"/>
                        <Label Name="iSpUrll" Content="Site URL" HorizontalAlignment="Left" Margin="362,6,0,0" VerticalAlignment="Top" Width="96" Height="30" ToolTip="The URL to your sharepoint site, for example: https://ogd.sharepoint.com/site1"/>
                        <TextBox Name="iLibName" HorizontalAlignment="Left" Height="23" TextWrapping="Wrap" Text="" VerticalAlignment="Top" Width="347" Margin="10,36,0,0" ToolTip="The name of the document library to upload to, for example: Documents (can be found in Library Properties)"/>
                        <Label Name="iLibNamel" Content="Library Name" HorizontalAlignment="Left" Margin="362,35,0,0" VerticalAlignment="Top" Width="96" Height="29" ToolTip="The name of the document library to upload to, for example: Documents (can be found in Library Properties)"/>
                        <TextBox Name="iFolderPath" HorizontalAlignment="Left" Height="23" TextWrapping="Wrap" Text="" VerticalAlignment="Top" Width="347" Margin="10,67,0,0"/>
                        <Button Name="iBrowseFolder" Content="Select folder" HorizontalAlignment="Left" Margin="362,67,0,0" VerticalAlignment="Top" Width="96" Height="23"/>
                        <Button Name="iVerifyFolder" Content="Scan &amp; Fix" HorizontalAlignment="Left" Margin="10,141,0,0" VerticalAlignment="Top" Width="80" Height="23" ToolTip="Scan will first correct all folder and file names in the given path to comply with Sharepoint Online standards."/>
                        <Button Name="iVerifyFolderNoCommit" Content="Scan Only" HorizontalAlignment="Left" Margin="120,141,0,0" VerticalAlignment="Top" Width="80" Height="23" ToolTip="Scan will first correct all folder and file names in the given path to comply with Sharepoint Online standards."/>
                        <Button Name="iUploadFolder" Content="Upload" HorizontalAlignment="Left" Margin="230,141,0,0" VerticalAlignment="Top" Width="80" Height="23"/>
                        <TextBox Name="iSpSubFolder" HorizontalAlignment="Left" Height="23" TextWrapping="Wrap" Text="" VerticalAlignment="Top" Width="347" Margin="10,98,0,0" ToolTip="If you do not wish to upload to the root of the document Library, enter the name of a subfolder here"/>
                        <Label Name="iSpSubFolderLabel" Content="Subfolder name" HorizontalAlignment="Left" Margin="362,95,0,0" VerticalAlignment="Top" Width="96" Height="29" ToolTip="If you do not wish to upload to the root of the document Library, enter the name of a subfolder here"/>
                    </Grid>
                </TabItem>
            </TabControl>
        </Grid>
    </Window>

"@

$XMLreader=(New-Object System.Xml.XmlNodeReader $xaml)
$communicatieObject.Interface.Window=[Windows.Markup.XamlReader]::Load( $XMLreader )
$xaml.SelectNodes("//*[@Name]") | %{
    $varname = $_.Name
    $communicatieObject.Interface.$varname = $communicatieObject.Interface.Window.FindName($varname)
    $communicatieObject.Interface.$varname.Dispatcher.Invoke("Render", [Windows.Input.InputEventHandler] {$communicatieObject.Interface.$varname.UpdateLayout()}, $null, $null)
}

#Browse for CSV button
$communicatieObject.Interface.iBrowseCSV.add_Click({
    $FileBrowser = New-Object System.Windows.Forms.OpenFileDialog
    [void]$FileBrowser.ShowDialog()
    $communicatieObject.Interface.iCSVPath.Text = $FileBrowser.FileName
    if($FileBrowser.FileName){$communicatieObject.functionCalls.hdCSVCheck = 1}
})

#Browse for Folder button
$communicatieObject.Interface.iBrowseFolder.add_Click({
    $FolderBrowser = New-Object System.Windows.Forms.FolderBrowserDialog
    [void]$FolderBrowser.ShowDialog()
    $communicatieObject.Interface.iFolderPath.Text = $FolderBrowser.SelectedPath
})

#Verify folder button
$communicatieObject.Interface.iVerifyFolder.add_Click({
    if($communicatieObject.status.folderVerifying -gt 0){
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Verification is already running", "O365Migrator" , 0)
    }else{
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("O365Migrator will now use O365DataCleaner to ensure source compatibility with Sharepoint Online. For more info see: http://www.lieben.nu/liebensraum/o365datacleaner/`n`nDo you wish to continue?", "Warning" , 4)
        if($OUTPUT -eq "YES"){
            if($communicatieObject.Interface.iFolderPath.Text){
                $communicatieObject.functionCalls.folderVerify = 1
            }else{
                $OUTPUT= [System.Windows.Forms.MessageBox]::Show("You need to select a folder first", "O365Migrator" , 0)
            }
        }
    }
})

#Verify folder button without commit
$communicatieObject.Interface.iVerifyFolderNoCommit.add_Click({
    if($communicatieObject.status.folderVerifyingNoCommit -gt 0){
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Verification is already running", "O365Migrator" , 0)
    }else{
        if($communicatieObject.Interface.iFolderPath.Text){
            $communicatieObject.functionCalls.folderVerifyNoCommit = 1
        }else{
            $OUTPUT= [System.Windows.Forms.MessageBox]::Show("You need to select a folder first", "O365Migrator" , 0)
        }
    }
})

#Verify homedirectories button
$communicatieObject.Interface.iVerifyCSV.add_Click({
    if($communicatieObject.status.hdVerifying -gt 0){
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Verification is already running", "O365Migrator" , 0)
    }else{
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("O365Migrator will now use O365DataCleaner to ensure source compatibility with Onedrive for Business. For more info see: http://www.lieben.nu/liebensraum/o365datacleaner/`n`nDo you wish to continue?", "Warning" , 4)
        if($OUTPUT -eq "YES"){
            if($communicatieObject.status.hdCSVimported -eq 1){
                $communicatieObject.functionCalls.hdVerify = 1
            }else{
                $OUTPUT= [System.Windows.Forms.MessageBox]::Show("You need to import a valid CSV file first", "O365Migrator" , 0)
            }
        }
    }
})

#Verify homedirectories button in scan only mode
$communicatieObject.Interface.iVerifyCSVNoCommit.add_Click({
    if($communicatieObject.status.hdVerifyingNoCommit -gt 0){
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Verification is already running", "O365Migrator" , 0)
    }else{
        if($communicatieObject.status.hdCSVimported -eq 1){
            $communicatieObject.functionCalls.hdVerifyNoCommit = 1
        }else{
            $OUTPUT= [System.Windows.Forms.MessageBox]::Show("You need to import a valid CSV file first", "O365Migrator" , 0)
        }
    }
})

#Upload homedirectories button
$communicatieObject.Interface.iUploadUser.add_Click({
    if($communicatieObject.status.hdUploading -gt 0){
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Upload is already running", "O365Migrator" , 0)
    }elseif($communicatieObject.status.o365connected -ne 1){
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Please connect to Office 365 before trying this", "O365Migrator" , 0)
    }else{
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("O365Migrator will now start uploading all homedirectories to Onedrive for Business. Existing files with the same name will be overwritten.`n`nDo you wish to continue?", "Warning" , 4)
        if($OUTPUT -eq "YES"){
            if($communicatieObject.status.hdCSVimported -eq 1){
                $communicatieObject.status.hdUploading_Starttime = Get-Date
                $communicatieObject.functionCalls.hdUpload = 1
            }else{
                $OUTPUT= [System.Windows.Forms.MessageBox]::Show("You need to import a valid CSV file first", "O365Migrator" , 0)
            }
        }
    }
})

#Upload folder button
$communicatieObject.Interface.iUploadFolder.add_Click({
    if($communicatieObject.status.folderUploading -gt 0){
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Upload is already running", "O365Migrator" , 0)
    }elseif($communicatieObject.status.o365connected -ne 1){
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Please connect to Office 365 before trying this", "O365Migrator" , 0)
    }else{
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("O365Migrator will now start uploading all data in the selected folder. Existing files with the same name will be overwritten.`n`nDo you wish to continue?", "Warning" , 4)
        if($OUTPUT -eq "YES"){
            $go = $true
            if($communicatieObject.status.folderVerified -ne 1){
                $go = $false
                $OUTPUT= [System.Windows.Forms.MessageBox]::Show("You have not run verify yet, this can cause upload failures.`n`nARE YOU REALLY SURE?", "Warning" , 4)
                if($OUTPUT -eq "YES"){
                    $go = $true
                }
            }
            if($go){
                if($communicatieObject.Interface.iSpURL.Text -and $communicatieObject.Interface.iLibName.Text -and $communicatieObject.Interface.iFolderPath.Text){
                    $communicatieObject.status.folderUploading_Starttime = Get-Date
                    $communicatieObject.functionCalls.folderUpload = 1
                }else{
                    $OUTPUT= [System.Windows.Forms.MessageBox]::Show("You need to specify a Sharepoint (site) URL, a library name and a source folder path", "O365Migrator" , 0)
                }
            }
        }
    }
})

#Connect to Office 365 button
$communicatieObject.Interface.iConnect.add_Click({
    if($communicatieObject.Interface.iLoginBox.Text -and $communicatieObject.Interface.iPasswordBox.Password){
        #loopback here
        $communicatieObject.functionCalls.connectToOffice365 = 1
        $communicatieObject.Interface.iConnectResult.Content = "Connecting..."
        $communicatieObject.Interface.iConnectResult.Foreground = "Orange"
    }else{
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Please enter your Office 365 login and password before connecting", "O365Migrator" , 0)
    }
})

#Provision users button
$communicatieObject.Interface.iProvision.add_Click({
    if($communicatieObject.status.o365connected -eq 1){
        #loopback here
        $communicatieObject.functionCalls.provisionO4B = 1
        $communicatieObject.Interface.iProvisionResult.Content = "Working..."
        $communicatieObject.Interface.iProvisionResult.Foreground = "Orange"
    }else{
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Please connect to Office 365 before trying this", "O365Migrator" , 0)
    }
})

#Set permissions for users button
$communicatieObject.Interface.iAuthorize.add_Click({
    if($communicatieObject.status.o365connected -eq 1){
        #loopback here
        $communicatieObject.functionCalls.authorizeO4B = 1
        $communicatieObject.Interface.iAuthorizeResult.Content = "Working..."
        $communicatieObject.Interface.iAuthorizeResult.Foreground = "Orange"
    }else{
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Please connect to Office 365 before trying this", "O365Migrator" , 0)
    }
})

#Remove permissions for users button
$communicatieObject.Interface.iunAuthorize.add_Click({
    if($communicatieObject.status.o365connected -eq 1){
        #loopback here
        $communicatieObject.functionCalls.unAuthorizeO4B = 1
        $communicatieObject.Interface.iAuthorizeResult.Content = "Working..."
        $communicatieObject.Interface.iAuthorizeResult.Foreground = "Orange"
    }else{
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Please connect to Office 365 before trying this", "O365Migrator" , 0)
    }
})

$communicatieObject.Interface.Window.ShowDialog() | Out-Null

return $True
