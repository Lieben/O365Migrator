﻿######## 
#o365Datacleaner v1.13
#Copyright:     Free to use, please leave this header intact 
#Author:        Jos Lieben 
#Script help:   www.lieben.nu
#Purpose:       This script makes the supplied folder path fully compliant with Onedrive for Business and/or Sharepoint Online standards
#Assumption:    When syncing with the target location, you have to use the new client (Onedrive.exe). If you're still using groove.exe minor modifications to the script may be necessary

###PARAMETERS
#Parameter1: -path path to the root of the folder structure that needs to be made compliant (e.g. \\fileserver\homedirs$\user1)
#Parameter2: -username username in Office 365, used to shorten the maximum folder depth when migrating to Onedrive for Business. Not used if omitted 
#Parameter3: -adminLog full path and name of the location to log to
#Parameter4: -commit 1 for read-write mode, 0 for read-only mode
#Return values: will return $False if the script failed, will return $True if successfull, and will return an error string if unable to log to the log file

Param(
    [Parameter(Mandatory=$true)][Hashtable]$communicatieObject,
    [Parameter(Mandatory=$true)][string]$path,
    [Parameter(Mandatory=$false)][string]$username,
    [Parameter(Mandatory=$true)][string]$adminLog,
    [Parameter(Mandatory=$true)][int]$commit,
    [Parameter(Mandatory=$true)][int]$mode, #mode = 0 means full functionality, 1 = no path shortening (and thus no O4BMoved folder), 2 = no registration of file sizes to the motherscript
    [Parameter(Mandatory=$true)][String]$folderIgnoreList
)

#'Static' Configuration:
$movedDirName = "O4BMoved" #this folder will contain files that were nested too deeply
$skippedDirName = "Non-Migrateable" #this folder will contain files that cannot be moved to Onedrive for Business or Sharepoint Online
$maxFileSize = 15700000000 # = +/- 15GB
$maxItems = 30000000 #maximum number of items allowed (O4B/SharepointOnline limit)
$maxDepth = 254 #maximum number of characters allowed in the path including the username
$cropFileFrom = 120 #length of a file name to trigger cropping 
$cropFileTo = 90 #length to crop a file name to if it is over cropFileFrom length (note: cropping starts at character 0)
$cropFolderFrom = 120 #length of a folder name to trigger cropping
$cropFolderTo = 90 #length to crop a folder name to if it is over cropFolderFrom length (note: cropping starts at character 0)
$maxTries = 5 #number of times the script will attempt to rename/move/delete etc
$timeBetweenRetries = 500 #milliseconds to wait before trying a failed operation again
$findUniqueTries = 99999 #maximum integer to append to a file or folder name to make it unique (when cropped or moved)
$writeUserLog = $True #Write a HTML log file to the Non-Migrateable folder. This is most useful for homedirectories
$originalDriveletter = "U:" #original driveletter you're migrating to Onedrive for Business or Sharepoint Online, this is used for the HTML report
$destinationName = "Office 365" #Used for the HTML report, set this to Sharepoint Online if you're migrating to Sharepoint Online instead of O4B

#Don't touch anything below this line
$debug = $False
$script:adminLogBuffer = ""
$script:userLogBuffer = ""
$script:folderMoveBuffer = @()
#$maxDepth -= $username.Length #no longer needed
$maxDepth += ($path.Length) #add the root path, since this will not be part of the new path
$script:items_found = 0
$skippedDirPath = Join-Path -Path $path -ChildPath $skippedDirName
$script:movedDirPath = $skippedDirPath
$userLog = Join-Path $path -ChildPath "MovedFilesAndFoldersReport.html"
#define what to search for
$illegalFileExtensions = '' #USE LOWER CASE HERE OR THIS MAY NOT MATCH! Currently here: ONE OneNote files
$deleteFileNameStrings = ''
#$replaceFileAndFolderNameStrings = '._,\,/,:,*,?,",<,>,|,#,%' #Uncomment for Groove.exe (old client)
$replaceFileAndFolderNameStrings = '#,%,<,>,:,",|,?,*,/' #Uncomment for Onedrive.exe (new client)
$replaceFilePrefixes = ".lock,CON,PRN,AUX,NUL,COM1,COM2,COM3,COM4,COM5,COM6,COM7,COM8,COM9,LPT1,LPT2,LPT3,LPT4,LPT5,LPT6,LPT7,LPT8,LPT9"
$replaceFolderNames = "_t,_w"
$moveFileNameStrings = '.files,~$'
$moveFoldernameStrings = '_files,-Dateien,_fichiers,_bestanden,_file,_archivos,-filer,_tiedostot,_pliki,_soubory,_elemei,_ficheiros,_arquivos,_dosyalar,_datoteke,_fitxers,_failid,_fails,_bylos,_fajlovi,_fitxategiak,_vti_'      

if($commit -eq 1){
    $adminLogBuffer += "$path | VERBOSE | | | Starting v1.13 scan in Read-Write Mode | $(Get-Date)`r`n"
}else{
    $adminLogBuffer += "$path | VERBOSE | | | Starting v1.13 scan in Read-Only Mode | $(Get-Date)`r`n"
}

#if folders to ignore were defined, split into array
if($folderIgnoreList){
    $adminLogBuffer += "$path | VERBOSE | | | Folder Ignore List Items: $folderIgnoreList |"
    $folderIgnoreArray = $folderIgnoreList -Split ','
}

#Abort job function, flush log buffers to file before returning result to operator or job manager
function abort{
    Param(
        [switch]$failed,
        [switch]$writeUserLog,
        [switch]$writeAdminLog
    )
    $io_error = $False
    if($writeUserLog -and $commit -eq 1){
        $tries = 0
        while($True){
            $tries++
            try{ac $userLog $userLogBuffer -Encoding UTF8 -Force
                break
            }catch{
                if($tries -lt $maxTries){
                    continue
                }else{
                    $io_error = "user"
                    break
                }
            }
            Sleep -m $timeBetweenRetries
        }
    }
    if($writeAdminLog){
        $tries = 0
        while($True){
            $tries++
            try{ac $adminLog $adminLogBuffer -Encoding UTF8 -Force
                break
            }catch{
                if($tries -lt $maxTries){
                    Sleep -m $timeBetweenRetries
                    continue
                }else{
                    $io_error = "admin"
                    break
                }
            }
        }
    }
    $resultString = "Succeeded but"
    if($failed) { $resultString = "Failed, and"}
    if($io_error){Write-Output "$resultString failed to write $io_error log: $($Error[0])"}elseif($failed){Write-Output $False}else{Write-Output $True}
    Exit
}

#this function cleans a folder or file name and returns an array. Index 0 is the new name, Index 1 is the removed characters
function fixFileOrFolderName{
    Param(
        [string]$itemName
    )
    #remove known illegal character strings
    foreach($char in $replaceFileAndFolderNameStrings.Split(',')) { 
        if($itemName.Contains($char)){
            $chars_Found+=$char
            if($itemName.length -lt 2){
                $itemName += "0"
            }
            $itemName = $itemName.Replace($char,'')
        }
    }
    #remove illegal filenames
    foreach($char in $replaceFilePrefixes.Split(',')) { 
        $loc = $itemName.LastIndexOf(".")
        if($loc -ne -1){
            $tempName = $itemName.Substring(0,$loc)
            if($tempName -and $tempName -eq $char){
                $chars_Found+=$char
                if($tempName.length -lt 2){
                    $itemName = $itemName.Replace($char,'0') 
                }else{
                    $itemName = $itemName.Replace($char,'') 
                }      
            }   
        } 
    }

    #rename _t and _w folders
    foreach($char in $replaceFolderNames.Split(',')) { 
        if($itemName -eq $char){
            $chars_Found+=$char
            $itemName += "0"
            $itemName = $itemName.Replace($char,'')        
        }    
    }
    
    #remove trailing dot
    if($itemName.EndsWith(".")){
        $chars_Found+="trailing dot"
        if($itemName.length -lt 2){
            $itemName += "0"
        }
        $itemName = $itemName.Substring(0,$itemName.Length-1)
    }
    #remove trailing or leading spaces
    if($itemName.EndsWith(" ") -or $itemName.StartsWith(" ")){
        $chars_Found+="leading or trailing space"
        if($itemName.length -lt 2){
            $itemName += "0"
        }
        $itemName = $itemName.Trim()
    }
    $itemName
    $chars_Found
}

#check if the O4BMoved folder exists within the given folder, if it does not exist create it, if unable to create it, abort and write an error to the admin log
function checkO4BMovedFolder{
    Param(
        [string]$currentPath
    )
    if([Alphaleonis.Win32.Filesystem.Directory]::Exists((Join-Path -Path $currentPath -ChildPath $movedDirName)) -eq $False){
        $tries = 0
        while($True){
            $tries++
            try{
                if($commit -eq 1) {
                    new-item (Join-Path -Path $currentPath -ChildPath $movedDirName) -itemtype Directory -ErrorAction Stop | Out-Null
                }
                $script:adminLogBuffer += "$path | VERBOSE | | | Created $movedDirName folder in $currentPath | `r`n"
                break
            }catch{
                if($tries -lt $maxTries){
                    Sleep -m $timeBetweenRetries
                    continue
                }else{
                    if($debug){
                        Write-Host "Failed to create O4BMoved folder" -ForegroundColor Red
                    }
                    $script:adminLogBuffer += "$path | CRITICAL ERROR | | | Failed to write $movedDirName folder in $currentPath | $($Error[0]) `r`n"
                    return $False
                }
            }
        }
    }
    return $True
}

#Check if the homedirectory exists, exit if it does not exist
if([Alphaleonis.Win32.Filesystem.Directory]::Exists($path) -eq $False){
    if($debug){
        Write-Host "Failed to access $path!" -ForegroundColor Red
    }
    $adminLogBuffer += "$path | CRITICAL ERROR | | | Failed to access $path | `r`n"
    abort -failed -writeAdminLog
}

#check if the Non-Migrateable folder exists, if it does not exist create it, if unable to create it, abort and write an error to the admin log
if([Alphaleonis.Win32.Filesystem.Directory]::Exists($skippedDirPath) -eq $False){
    $tries = 0
    while($True){
        $tries++
        try{
            if($commit -eq 1){
                new-item $skippedDirPath -itemtype Directory -ErrorAction Stop | Out-Null
                if($mode -eq 0) {
                    ac $userLog "<html><head><meta http-equiv=`"Content-Type`" content=`"text/html; charset=UTF-8`"/><title>Moved Files and Folders Report</title></head><body><h1>Moved Files and Folders Report</h1><table border=`"1`"><tr><td><b>Previous location</b></td><td><b>New location</b></td><td><b>Reason for Move</b></td></tr>" -ErrorAction Stop | Out-Null
                }
            }
            $adminLogBuffer += "$path | VERBOSE | | | Created $skippedDirName folder | `r`n"
            break
        }catch{
            if($tries -lt $maxTries){
                Sleep -m $timeBetweenRetries
                continue
            }else{
                if($debug){
                    Write-Host "Failed to create Non-Migrateable folder!" -ForegroundColor Red
                }
                $adminLogBuffer += "$path | CRITICAL ERROR | | | Failed to write $skippedDirName folder | $($Error[0])`r`n"
                abort -failed -writeAdminLog
            }
        }
    }
}

#Create O4BMoved folder if it doesn't exist yet
if($mode -eq 0){
    $folderresult = checkO4BMovedFolder -currentPath $path
    if($folderresult){
        $script:movedDirPath = Join-Path -Path $path -ChildPath $movedDirName
    }
}
function addToMoveList{
    Param(
        [Parameter(Mandatory=$true)]
        [string]$folderPath
    )
    $folderStructure = @()
    $parent = Split-Path -Path $folderPath -Parent
    while($True){
        if($parent -eq ""){
            break
        }
        $folderStructure+=$parent
        $parent = Split-Path -Path $parent -Parent
    }
    [Array]::Reverse($folderStructure)
    $already_added = $False
    foreach($folder in $folderStructure){
        if($folderMoveBuffer -contains $folder){
            $already_added = $True
        }
    }
    if($already_added -eq $False){
        $script:folderMoveBuffer += (Split-Path -Path $folderPath -Parent)
    }
}


#this function will check if a path already exists, if it does not, it will return the same path, otherwise it will append a number to make it unique
function returnUniqueName{
    Param(
        [Parameter(Mandatory=$true)]
        [string]$path,
        [Parameter(Mandatory=$true)]
        [string]$originalPath
    )
    $metaData = ""
    try{
        $metaData = [Alphaleonis.Win32.Filesystem.FileInfo]($originalPath)
    }catch{
        return $False
    }
    $attempt = 0
    $exists = $False
    $originalPath = $path
    while($attempt -le $findUniqueTries){
        if($attempt -gt 0){
            if($metaData.Extension.Length -lt 1){
                $path = "$($originalPath)$($attempt)"
            }else{
                try{
                    $extension = $metaData.Extension
                    $path = $originalPath.Substring(0,$originalPath.LastIndexOf("$extension"))
                    $path = "$($path)$($attempt)$($extension)"
                }catch{
                   $path = "$($originalPath)$($attempt)" 
                }
            }
        }
        if($metaData.Attributes -Match "directory"){
            $exists = [Alphaleonis.Win32.Filesystem.Directory]::Exists($path)
        }else{
            $exists = [Alphaleonis.Win32.Filesystem.File]::Exists($path)
        }
        if($exists -eq $False){
            return $path
        }
        $attempt++
    }

    #could not find a unique name
    if($attempt -ge $findUniqueTries){
        return $False
    }
}

#calling this function will move the file or folder to the specified directory, returns the new full path
#if it fails, it will exit the script
function moveToDir{
    Param(
        [Parameter(Mandatory=$true)]
        [string]$dirPath, #path to target directory
        [Parameter(Mandatory=$true)]
        [string]$oldPath, #full path to file or folder
        [Parameter(Mandatory=$true)]
        [string]$itemName, #name of target file or folder
        [Parameter(Mandatory=$true)]
        [string]$reason,
        [Switch]$folder
    )

    $tries = 0
    $succeeded = $False
    if($debug){
        Write-Host "Moving $oldPath to $(Join-Path -Path $dirPath -ChildPath $itemName) because $reason"
    }
    while($maxTries -gt $tries){
        $tries++ 
        #set new path of the file or folder to the target file or folder
        $newFullPath = Join-Path -Path $dirPath -ChildPath $itemName
        $newFullPath = returnUniqueName -path $newFullPath -originalPath $oldPath
        #Check if the returned value is not False, then do the rename
        if($False -ne $newFullPath){
                try{
                    $moveOptions = [Alphaleonis.Win32.Filesystem.MoveOptions]::ReplaceExisting
                    #only actually move the file if -commit was specified
                    if($commit -eq 1){

                        if($folder){
                            [Alphaleonis.Win32.Filesystem.Directory]::Move($oldPath,$newFullPath,$moveOptions)
                        }else{
                            [Alphaleonis.Win32.Filesystem.File]::Move($oldPath,$newFullPath,$moveOptions)
                        }
                    }
                    #prep strings for User and Admin logs
                    $oldPathForUserLog = $oldPath.Replace($path,$originalDriveletter)
                    if($dirPath -Like "*$skippedDirName*"){
                        $newPathForUserLog = $newFullPath.Replace($path,$originalDriveletter)
                    }else{
                        $newPathForUserLog = $newFullPath.Replace($path,$destinationName)
                    }
                    $script:userLogBuffer += "<tr><td>$oldPathForUserLog</td><td>$newPathForUserLog</td><td>$reason</td>`r`n"
                    $script:adminLogBuffer += "$path | VERBOSE | $oldPath | $newFullPath | moved | $reason `r`n"
                    $succeeded = $True
                    break
                }catch{
                    Sleep -m $timeBetweenRetries
                    continue
                }
        }else{
            Sleep -m $timeBetweenRetries
            continue
        }   
    }
    if($succeeded){
        return $newFullPath
    }else{
        $msgString = ""
        if($False -eq $newFullPath){$msgString = ", could not find a new unique name"}
        if($debug){
            Write-host "moving $oldPath to $(Join-Path -Path $dirPath -ChildPath $itemName) FAILED$msgString" -ForegroundColor Red
        }
        $script:adminLogBuffer += "$path | ERROR | $oldPath | $(Join-Path -Path $dirPath -ChildPath $itemName) | could not be moved$msgString | $($Error[0])`r`n"
        return $False
    }
}

#this function will rename a file or folder to the new name specified, it will return the new path to the file or folder
function renameFileOrFolder{
    Param(
        [Parameter(Mandatory=$true)]
        [string]$newPath,
        [Parameter(Mandatory=$true)]
        [string]$oldPath,
        [Parameter(Mandatory=$true)]
        [string]$reason
    )
    $tries = 0
    $succeeded = $False
    if($debug){
        write-host "$oldPath will be renamed to $newPath because $reason"
    }
    while($maxTries -gt $tries){
        $tries++ 
        $newFullPath = returnUniqueName -path $newPath -originalPath $oldPath
        #Check if the returned value is not False, then do the rename, else increment the attempt counter
        if($False -ne $newFullPath){
            try{
                $objectInfo = [Alphaleonis.Win32.Filesystem.DirectoryInfo]($oldPath)
                $moveOptions = [Alphaleonis.Win32.Filesystem.MoveOptions]::ReplaceExisting
                #only actually rename the file if -commit was specified
                if($commit -eq 1){
                    if($objectInfo.Exists){
                        [Alphaleonis.Win32.Filesystem.Directory]::Move($oldPath,$newFullPath,$moveOptions)
                    }else{
                        [Alphaleonis.Win32.Filesystem.File]::Move($oldPath,$newFullPath,$moveOptions)
                    }    
                    #Move-Item -LiteralPath $oldPath -Destination $newFullPath -ErrorAction Stop -Force
                }
                $script:adminLogBuffer += "$path | VERBOSE | $oldPath | $newFullPath | renamed | $reason `r`n"
                $succeeded = $True
                break
            }catch{
                Sleep -m $timeBetweenRetries
                continue
            }
        }
    }
    if($succeeded){
        return $newFullPath
    }else{
        $msgString = ""
        if($False -eq $newFullPath){$msgString = ", could not find a new unique name"}
        if($debug){
            Write-host "renaming $oldPath to $newPath FAILED$msgString"
        }
        $script:adminLogBuffer += "$path | ERROR | $oldPath | $newPath | could not be renamed$msgString | $($Error[0]) `r`n"
        return $False
    }
}

function cleanFolder{
    Param(
        [Parameter(Mandatory=$true)]
        [string]$folderPath
    )
    $tries = 0
    $folderBuffer = @()
    while($True){
        $tries++
        try{
            $SearchOption = 'TopDirectoryOnly'
            $array = @()
            $items = @()
	        $FileSystemEntryInfo = @()
            $SearchPattern = '*'
	        $array = [Alphaleonis.Win32.Filesystem.Directory]::EnumerateDirectories($folderPath,$SearchPattern,[System.IO.SearchOption]::$SearchOption)
		    Foreach ($file in $array) { $items += $file } 
	        $array = [Alphaleonis.Win32.Filesystem.Directory]::EnumerateFiles($folderPath,$SearchPattern,[System.IO.SearchOption]::$SearchOption)
		    Foreach ($file in $array) { $items += $file } 
	        foreach($item in $items){
		        $folderBuffer += [Alphaleonis.Win32.Filesystem.File]::GetFileSystemEntryInfo($item) #FullPath = FullName, FileName = Name, FileSize = Length, no extension 
	        }
            #$folderBuffer = Get-ChildItem -LiteralPath $folderPath -Force | select-object FullName, Name, PSIsContainer, Length, Extension
            break
        }catch{
            if($tries -ge $maxTries){
                $script:adminLogBuffer += "$path | ERROR | $folderPath | | could not be loaded | $($Error[0]) `r`n"
                return $False
            }
            Sleep -m $timeBetweenRetries
            continue
        }
    }

    #Skip empty folders
    if(-not $folderBuffer){
        $script:adminLogBuffer += "$path | VERBOSE | $folderPath | | skipped because folder is empty |`r`n"
        return $True
    }

    #check if we are in a first level subfolder
    $parentPath = Split-Path -Path $folderPath -Parent
    $currentFolder = Split-Path -Path $folderPath -Leaf
    if($parentPath -eq $path -or "$($parentPath)\" -eq $path){
        #Skip folders in the Ignore List if any
        if($currentFolder -and $folderIgnoreArray -contains $currentFolder){
            $script:adminLogBuffer += "$path | VERBOSE | $currentFolder | | skipped because the folder name is on the ignore list |`r`n"
            return $True
        }                
    }

    #loop over the contents
    foreach($child in $folderBuffer){
        $script:items_found++ #increment the counter of folders/files found for the max items rule
        $pathBuffer = $child.FullPath
        $nameBuffer = $child.FileName
        $metaData = [Alphaleonis.Win32.Filesystem.FileInfo]($pathBuffer)
        $extensionBuffer = $metaData.Extension
        $sizeBuffer = $child.FileSize
        #check if we haven't exceeded the maximum number of items
        if($script:items_found -ge $maxItems){
            #we cannot add more files to Onedrive for Business as we have exceeded the limit, move everything else we find to the skipped folder
            $null = moveToDir -dirPath $skippedDirPath -oldPath $pathBuffer -itemName $nameBuffer -reason "maximum number of $maxItems reached"
            #use 'continue' to skip all further rules and move to the next item
            continue
        }

        #check if any illegal file extensions are present, and move the file if so
        if($child.IsDirectory -eq $False){
            $found_string = $False
            foreach($extension in $illegalFileExtensions.Split(',')) { 
                if($extension -and $extensionBuffer.ToLower() -eq $extension){
                    $found_string = $True
                    $null = moveToDir -dirPath $skippedDirPath -oldPath $pathBuffer -itemName $nameBuffer -reason "extension $extension is not allowed"
                    break      
                }
            }
            #stop processing rules for this item since it was moved
            if($found_string){
                continue
            }
        }
        #Check if this file needs to be moved outside O4B
        if($child.IsDirectory -eq $False){
            #check for files that need to be moved outside of Onedrive for Business because they cannot be synced
            #files that match specific illegal strings, will need to be moved to the non-migrateable folder
            $found_string = $False
            foreach($string in $moveFileNameStrings.Split(',')) { 
                if($string -and $nameBuffer.Contains($string)){
                    $found_string = $True
                    #call move function
                    $newFullPath = moveToDir -dirPath $skippedDirPath -oldPath $pathBuffer -itemName $nameBuffer -reason "name contained $string"
                    $script:items_found-- #since this item was counted previously, we should substract it since it has been moved
                    break                
                }
            }
            #stop processing rules for this item since it was moved to the skipped folder
            if($found_string){
                continue
            }
        }
        if($child.IsDirectory -eq $False){
            #Rule 1: crop files with a too long filename, include extension
            if($nameBuffer.Length -ge $cropFileFrom -and $mode -eq 0){
                $newName = "";$newPath = ""
                $newName = $nameBuffer.SubString(0,($cropFileTo-($extensionBuffer.Length)))
                $newName = "$($newName)$($extensionBuffer)"
                $newPath = Join-Path -Path (Split-Path -Path $pathBuffer -Parent) -ChildPath $newName
                $newFullPath = renameFileOrFolder -newPath $newPath -oldPath $pathBuffer -reason "filename too long"
                #don't actually use the new file or folder name if we aren't running in read-write and there was no error, or we won't be able to find the new name
                if($commit -eq 1 -and $False -ne $newFullPath){
                    $nameBuffer = Split-Path -Path $newFullPath -Leaf
                    $pathBuffer = $newFullPath
                }
            }
            #Rule 2: move files with a too long path
            if($pathBuffer.Length -ge $maxDepth -and $mode -eq 0){
                $newFullPath = moveTodir -dirPath $movedDirPath -oldPath $pathBuffer -itemName $nameBuffer -reason "file is nested too deeply in subfolders"
                #don't actually use the new file or folder name if we aren't running in read-write and there was no error, or we won't be able to find the new name
                if($commit -eq 1 -and $False -ne $newFullPath){
                    $nameBuffer = Split-Path -Path $newFullPath -Leaf
                    $pathBuffer = $newFullPath
                }
            }
        }else{
        #it's a folder
            #Rule 3: crop folders with a too long name
            if($nameBuffer.Length -ge $cropFolderFrom -and $mode -eq 0){
                $newName = "";$newPath = ""
                $newName = $nameBuffer.SubString(0,$cropFolderTo)
                $newPath = Join-Path -Path (Split-Path -Path $pathBuffer -Parent) -ChildPath $newName
                $newFullPath = renameFileOrFolder -newPath $newPath -oldPath $pathBuffer -reason "folder name too long"
                #don't actually use the new file or folder name if we aren't running in read-write and there was no error, or we won't be able to find the new name
                if($commit -eq 1 -and $False -ne $newFullPath){
                    $nameBuffer = Split-Path -Path $newFullPath -Leaf
                    $pathBuffer = $newFullPath
                }
            }
        }

        $newFullPath = $pathBuffer
        $renameNeeded = $False
        $chars_Found = ""
        #run 1 (remove illegal strings)
        $ret1 = fixFileOrFolderName -itemName $nameBuffer
        if($ret1[0] -ne $nameBuffer){
            $renameNeeded = $True
            #run 2 (in case the rename introduced additional illegal strings such as ..)
            $ret2 = fixFileOrFolderName -itemName $ret1[0]
            $chars_Found = "$($ret1[1])$($ret2[1])"
            $newFullPath = Join-Path -Path (Split-Path -Path $newFullPath -Parent) -ChildPath $ret2[0]
        }
 
        #if any were found, rename the file or folder to the new name
        if($renameNeeded){
            $newFullPath = renameFileOrFolder -newPath $newFullPath -oldPath $pathBuffer -reason "$chars_Found found"
            #don't actually use the new file or folder name if we aren't running in read-write and there was no error, or we won't be able to find the new name
            if($commit -eq 1 -and $False -ne $newFullPath){
                $nameBuffer = Split-Path -Path $newFullPath -Leaf
                $pathBuffer = $newFullPath
            }
        }

        #If it's a file, process file specific stuff:
        if($child.IsDirectory -eq $False){
            #check for files that are too large
            if($sizeBuffer -ge $maxFileSize){
                $newFullPath = moveToDir -dirPath $skippedDirPath -oldPath $pathBuffer -itemName $nameBuffer -reason "file too large"
                #don't actually use the new file or folder name if we aren't running in read-write and there was no error, or we won't be able to find the new name
                if($commit -eq 1 -and $False -ne $newFullPath){
                    $nameBuffer = Split-Path -Path $newFullPath -Leaf
                    $pathBuffer = $newFullPath
                }
                $script:items_found-- #since this item was counted previously, we should substract it since it has been moved
                #move to the next item, other rules won't apply since it was moved to the skipped directory
                continue
            }

            #check for files that need to be deleted
            $found_string = $False
            if($deleteFileNameStrings){
                foreach($string in $deleteFileNameStrings.Split(',')) { 
                    #files that match, will need to be deleted
                    if($string -and $nameBuffer.Contains($string)){
                        $found_string = $True
                        $tries = 0
                        $succeeded = $False
                        while($maxTries -gt $tries){
                            $tries++
                            try{
                                #only actually delete the file if -commit was specified
                                if($commit -eq 1){
                                    if($debug){
                                        Write-host "delete $($pathBuffer)"
                                    }
                                    [Alphaleonis.Win32.Filesystem.File]::Delete($pathBuffer,$True)
                                    #Remove-Item -LiteralPath $pathBuffer -ErrorAction Stop -Force
                                }
                                $succeeded = $True
                                $script:adminLogBuffer += "$path | VERBOSE | $pathBuffer | | deleted | `r`n"
                                break
                            }catch{
                                if($debug){
                                    Write-host "delete $($pathBuffer) FAILED"
                                }
                                Sleep -m $timeBetweenRetries
                                continue
                            }
                        }
                        if($succeeded){
                            $script:items_found-- #since this item was counted previously, we should substract it since it has been moved
                            break  
                        }else{
                            $script:adminLogBuffer += "$path | ERROR | $pathBuffer | | could not be deleted | $($Error[0]) `r`n"
                            break
                        }    
                    }
                }
            }
            #stop processing rules for this item if it was deleted
            if($succeeded){
                continue
            }else{
                #count file size to the parent script variable
                if($mode -ne 2){
                    $communicatieObject.status.verifying_totalSize += $sizeBuffer
                    $communicatieObject.status.folderVerifyingItemCount++
                }
            }
        }
        #If it's a folder, process folder specific stuff:
        if($child.IsDirectory){
            $moved = $False
            foreach($string in $moveFoldernameStrings.Split(',')) { 
                #folders that match, will need to be moved to a non-migrateable folder
                if($string -and $nameBuffer.Contains($string)){
                    $moved = $True
                    $newFullPath = moveToDir -dirPath $skippedDirPath -oldPath $pathBuffer -itemName $nameBuffer -reason "name contained $string" -folder
                    #don't actually use the new file or folder name if we aren't running in read-write and there was no error, or we won't be able to find the new name
                    if($commit -eq 1){
                        if($False -ne $newFullPath){
                            $nameBuffer = Split-Path -Path $newFullPath -Leaf
                            $pathBuffer = $newFullPath
                        }else{
                            $moved = $False
                        }
                    }
                    $script:items_found-- #since this item was counted previously, we should substract it since it has been moved
                    break                
                }
            }
            #Rule 4: add a folder to the move list if its path is too long and a parent folder was not already added to the move list
            if($pathBuffer.Length -ge $maxDepth -and $mode -eq 0){
                $null = addToMoveList -folderPath $pathBuffer
            }
            #if there is no match, we'll have to inspect the contents of this folder as well, using the new name when running in Read-Write mode
            if($moved -eq $False){
                $null = cleanFolder -folderPath $pathBuffer
            }else{
                Continue #stop processing further rules since this item was moved to the skipped directory
            }
        }   
    }
}



#Run 1: process all cleanup rules and fill the Move List
$null = cleanFolder -folderPath $path

#Run 2: process the folder Move List
foreach ($dir in $folderMoveBuffer){
    $name = Split-Path -Path $dir -Leaf
    if($name.Length -lt 1){
        $name = $dir
    }
    $null = moveToDir -dirPath $skippedDirPath -oldPath $dir -itemName $name -reason "folder is nested too deeply" -folder
}

$adminLogBuffer += "$path | VERBOSE | | | Finished scan | $(Get-Date) `r`n"

#Exit the script and flush all logs
if($mode -eq 0){
    abort -writeUserLog -writeAdminLog
}else{
    abort -writeAdminLog
}



