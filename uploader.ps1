﻿#Script name: O365Migrator
#Author: Jos Lieben
#Date: 04-08-2017
#Script help: www.lieben.nu

Param(
    [Parameter(Mandatory=$true)]
    [Hashtable]$communicatieObject,
    [Parameter(Mandatory=$true)]
    [string]$o365username,
    [Parameter(Mandatory=$true)]
    [string]$o365password,
    [Parameter(Mandatory=$true)]
    [string]$sourceFolder,        
    [Parameter(Mandatory=$true)]
    [string]$log,
    [Parameter(Mandatory=$true)]
    [string]$targetURL,
    [Parameter(Mandatory=$true)]
    [string]$baseScriptPath,
    [Parameter(Mandatory=$true)]
    [int]$cleanMode,
    [String]$libraryName,
    [String]$specificFolder,
    [String]$targetFolder,
    [String]$folderIgnoreList,
    $username
)
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint.Client") | Out-Null[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint.Client.Runtime") | Out-Null[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint.Client.UserProfiles") | Out-Null

$folderName = Split-Path -Path $sourceFolder -Leaf

$adminLog = $env:APPDATA+"\O365MigratorDataUploaderResults.log"
$script:adminLogBuffer = ""
$adminLogBuffer += "$sourceFolder | VERBOSE | | | Folder Ignore List Items: $folderIgnoreList |"
Write-Host "$sourceFolder folder Ignore List loaded: $folderIgnoreList" -ForegroundColor Green
$folderIgnoreArray = $folderIgnoreList -Split ','

$maxTries = 5 #number of times the script will attempt to save a file/log entry

if([System.IO.Directory]::Exists($sourceFolder)){
    $adminLogBuffer += "$sourceFolder | VERBOSE | Source folder exists | $(Get-Date)`r`n"
}else{
    $adminLogBuffer += "$sourceFolder | ERROR | $sourceFolder could not be found or accessed | $($Error[0])`r`n"
    Write-Host "$sourceFolder could not be found or accessed $($Error[0])" -ForegroundColor Red
    abort -failed -writeAdminLog
}


if($cleanMode -ne 3){
    $adminLogBuffer += "$sourceFolder | VERBOSE | Starting final verification | $(Get-Date)`r`n"
    try{
        #final attempt to clean up the source folder
        $subscript = "cleaner.ps1"
        $subscriptPath = Join-Path -Path $baseScriptPath -ChildPath $subscript
        & $subscriptPath -communicatieObject $communicatieObject -path $sourceFolder -username $o365username -adminLog $log -commit 1 -mode $cleanMode -folderIgnoreList $folderIgnoreList | Out-Null
    }catch{}
}

$adminLogBuffer += "$sourceFolder | VERBOSE | Starting upload to $targetURL | $(Get-Date)`r`n"


#Abort job function, flush log buffers to file before returning result to operator or job manager
function abort{
    Param(
        [switch]$failed,
        [switch]$writeAdminLog
    )
    try{
        if ($resetVersioning -eq $true){
            $script:adminLogBuffer += "$sourceFolder | VERBOSE | enabled versioning on this library`r`n" 
            write-host "enabled versioning on this library" 
            $library.EnableVersioning = $True
            $library.Update()
            $Context.ExecuteQuery()
        }
        if ($resetCheckout -eq $true){
            $script:adminLogBuffer += "$sourceFolder | VERBOSE | enabled forced checkout on this library`r`n" 
            write-host "enabled forced checkout on this library" 
            $library.ForceCheckout = $true
            $library.Update()
            $Context.ExecuteQuery()
        }
    }catch{write-host "failed to disable stuff again: $($Error[0])"}
    $io_error = $False
    if($writeAdminLog){
        $tries = 0
        while($True){
            $tries++
            try{ac $adminLog $adminLogBuffer -Encoding UTF8 -Force
                break
            }catch{
                if($tries -lt $maxTries){
                    Sleep -m $timeBetweenRetries
                    continue
                }else{
                    $io_error = "admin"
                    break
                }
            }
        }
    }

    $resultString = "Succeeded but"
    if($failed) { $resultString = "Failed, and"}
    if($io_error){Write-Output "$resultString failed to write $io_error log: $($Error[0])"}elseif($failed){Write-Output $False}else{Write-Output $True}
    Exit
}

Function Ensure-Folder(){
    Param(
      [Parameter(Mandatory=$True)]
      [Microsoft.SharePoint.Client.Web]$Web,
      [Parameter(Mandatory=$True)]
      [Microsoft.SharePoint.Client.Folder]$ParentFolder,
      [Parameter(Mandatory=$True)]$newFolder,
      [Switch]$root
    )
    try{
        if($root){
            $curFolder = $ParentFolder.Folders.Add($newFolder)
            $Web.Context.Load($curFolder)
            $web.Context.ExecuteQuery()
        }else{
            $curFolder = $ParentFolder.Folders.Add($newFolder.Name)
            $Web.Context.Load($curFolder)
            $web.Context.ExecuteQuery()
            $item = $curFolder.ListItemAllFields
            $item["Created"] = $newFolder.CreationTimeUtc
            $item["Modified"] = $newFolder.LastWriteTimeUtc
            $item["Author"] = $userLookupString
            $item["Editor"] = $userLookupString
            $item.Update()
            $Web.Context.ExecuteQuery()
        }
        $url_disp = $($curFolder.ServerRelativeUrl)
        $script:adminLogBuffer += "$sourceFolder | VERBOSE | New folder created or updated | $url_disp`r`n"  
        Write-Host "$sourceFolder : New folder created or updated: $url_disp" -ForegroundColor Green
        return $curFolder
    }catch{
        $script:adminLogBuffer += "$sourceFolder | ERROR | Failed to create new folder | $url_disp, $($Error[0])`r`n"  
        Write-Host "$sourceFolder : Failed to create new folder: $($Error[0])" -ForegroundColor Red
        $script:communicatieObject.status.Uploading_failedObjectCount++
        Throw "failed to create folder"
    }           
}

#Modified By: Alan Wallace (alan.wallace@oit.edu) 
#Modification Date: 8/1/2017
#Now performs chunked file uploads for files >10MB which prevents errors with SaveBinaryDirect for large files
#Chunked file upload ported to PowerShell from example at: https://msdn.microsoft.com/en-us/pnp_articles/upload-large-files-sample-app-for-sharepoint
Function Upload-File(){
    Param(
      [Parameter(Mandatory=$True)]
      [Microsoft.SharePoint.Client.Web]$Web,
      [Parameter(Mandatory=$True)]
      [String]$fileUrl, 
      [Parameter(Mandatory=$True)]
      [System.IO.FileInfo]$LocalFile
    )
    $fileChunkSizeInMB = 10
	$uploadTryCount = 3 #Number of times upload will be attempted in case of failure
    $Context = $Web.Context
    $exists = $False

    try{
        [Microsoft.SharePoint.Client.File] $existingFile = $Web.GetFileByServerRelativeUrl($fileUrl)
        $Context.Load($existingFile)
        $Context.ExecuteQuery()
        $fileDate = [datetime]$LocalFile.LastWriteTimeUtc
        $spDate = [datetime]$existingFile.TimeLastModified
        $difference = New-TimeSpan $fileDate $spDate
        if($difference.Hours -eq 0 -and $existingFile.Length -eq $LocalFile.Length){
            $exists = $True
        }
        $Context.ExecuteQuery()
    }catch{
        $exists = $False
    }
    if($exists -eq $False){
		$uploadCurrentTry = 1
		while($uploadCurrentTry -le $uploadTryCount){
			try{
				[Guid]$uploadId = [Guid]::NewGuid()
				[Microsoft.SharePoint.Client.File] $uploadFile = $null;
				[string]$uniqueFileName = $LocalFile.Name
				$fileName = $LocalFile.FullName
		
		
				[Microsoft.SharePoint.Client.File]$uploadFile
				[int]$blockSize = $fileChunkSizeInMB * 1024 * 1024
		
				$folderPath = $fileUrl.Substring(0, $fileUrl.LastIndexOf('/'))
				
				$library = $Web.GetFolderByServerRelativeUrl($folderPath)
				$Context.Load($library)
		
				$Context.ExecuteQuery()
				[long]$fileSize = $LocalFile.Length 
				if ($fileSize -le $blockSize){
					$fs = $null
					try{
						$fs = [System.IO.File]::Open($fileName, 'Open', 'Read', 'ReadWrite')
					
						[Microsoft.SharePoint.Client.FileCreationInformation]$fileInfo = (New-Object Microsoft.SharePoint.Client.FileCreationInformation)
						$fileInfo.ContentStream = $fs
						$fileInfo.Url = $uniqueFileName
						$fileInfo.Overwrite = $true
						$uploadFile = $library.Files.Add($fileInfo)
						$Context.Load($uploadFile)
						$Context.ExecuteQuery()
				
						$item = $uploadFile.ListItemAllFields
						$item["Created"] = $LocalFile.CreationTimeUtc
						$item["Modified"] = $LocalFile.LastWriteTimeUtc
						$item["Author"] = $userLookupString
						$item["Editor"] = $userLookupString
						$item.Update()
						$Context.ExecuteQuery()
					}
					finally{
						if($fs -ne $null){
							$fs.Dispose()
						}
					}
				}
				else{
					$bytesUploaded = $null
					$fs = $null
					try{
						$fs = [System.IO.File]::Open($fileName,'Open','Read', 'ReadWrite')
						$br = $null
						try{
							$br = (New-Object System.IO.BinaryReader $fs)
							[byte[]]$buffer = new-object byte[] $blockSize
							[byte[]]$lastBuffer = $null
							[long]$fileoffset = 0
							[long]$totalBytesRead = 0
							[int]$bytesRead = 0
							[bool]$first = $true
							[bool]$last = $false

							while(($bytesRead = $br.Read([byte[]]$buffer,[int]0,[long]$buffer.Length)) -gt 0){
								$totalBytesRead = $totalBytesRead + $bytesRead
								if ($totalBytesRead -eq $fileSize){
									$last = $true
									$lastBuffer = new-object byte[] $bytesRead;
									[Array]::Copy($buffer,0,$lastBuffer,0,$bytesRead)
								}
								if($first){
									$contentStream = $null
									try{
										$contentStream = (New-Object System.IO.MemoryStream)
										[Microsoft.SharePoint.Client.FileCreationInformation]$fileInfo = (New-Object Microsoft.SharePoint.Client.FileCreationInformation)
										$fileInfo.ContentStream = $contentStream
										$fileInfo.Url = $uniqueFileName
										$fileInfo.Overwrite = $true
										$uploadFile = $library.Files.Add($fileInfo)
										$s = $null
										try{
											$s = (New-Object System.IO.MemoryStream -ArgumentList @(,$buffer))
											$bytesUploaded = $uploadFile.StartUpload($uploadId,$s)
											$Context.ExecuteQuery()
											$fileoffset = $bytesUploaded.Value
										}
										finally{
											if($s -ne $null){
												$s.Dispose()
											}
										}
										$first = $false
									}
									finally{
										if($contentStream -ne $null){
											$contentStream.Dispose()
										}
									}
								}
								else{
									$uploadFile = $Context.Web.GetFileByServerRelativeUrl($library.ServerRelativeUrl + [System.IO.Path]::AltDirectorySeparatorChar + $uniqueFileName)
									if ($last){
										$s = $null
										try{
											$s = (New-Object System.IO.MemoryStream -ArgumentList  @(,$lastBuffer))
											$uploadFile = $uploadFile.FinishUpload($uploadId,$fileoffset,$s)
											$Context.ExecuteQuery()
											#Need to grab a fresh reference to the file
											$uploadFile = $Context.Web.GetFileByServerRelativeUrl($library.ServerRelativeUrl + [System.IO.Path]::AltDirectorySeparatorChar + $uniqueFileName)
											$Context.Load($uploadFile)
											$Context.ExecuteQuery()

											#Set file properties such as original creation/modification times and author/editor
											$item = $uploadFile.ListItemAllFields
											$item["Created"] = $LocalFile.CreationTimeUtc
											$item["Modified"] = $LocalFile.LastWriteTimeUtc
											$item["Author"] = $userLookupString
											$item["Editor"] = $userLookupString
											$item.Update()
											$Context.ExecuteQuery()
										}
										finally{
											if($s -ne $null){
												$s.Dispose()
											}
										}
									}else{
										$s = $null
										try{
											$s = (New-Object System.IO.MemoryStream -ArgumentList @(,$buffer))
											$bytesUploaded = $uploadFile.ContinueUpload($uploadId,$fileoffset,$s)
											$Context.ExecuteQuery()
											$fileoffset = $bytesUploaded.Value
										}
										finally{
											if($s -ne $null){
												$s.Dispose()
											}
										}
									}
								}
							}
						}
						finally{
							if($br -ne $null){
								$br.Dispose()
							}
						}
					}
					finally{
						if ($fs -ne $null){
							$fs.Dispose()
						}
					}
				}
				$script:adminLogBuffer += "$sourceFolder | VERBOSE | file uploaded | $fileUrl, size $($LocalFile.Length)`r`n" 
				Write-Host "$sourceFolder : File uploaded: $fileUrl, size $($LocalFile.Length)" -ForegroundColor Green       
				$communicatieObject.status.Uploading_ByteCount += $($LocalFile.Length)
				break
			}catch{
				if($uploadCurrentTry++ -eq $uploadTryCount){
					$script:adminLogBuffer += "$sourceFolder | ERROR | file upload failed | $fileUrl, $($Error[0])`r`n" 
					Write-Host "$sourceFolder : Failed to upload file: $($Error[0])" -ForegroundColor Red 
					$script:communicatieObject.status.Uploading_failedObjectCount++
					Throw "failed to upload"
				}
			}
		}
    }else{
        $script:adminLogBuffer += "$sourceFolder | VERBOSE | file skipped, already exists | $fileUrl, size $($LocalFile.Length)`r`n" 
        Write-Host "$sourceFolder : File skipped, already exists: $fileUrl, size $($LocalFile.Length)" -ForegroundColor Green       
        $communicatieObject.status.Uploading_ByteCount += $($LocalFile.Length)   
    }
}


 
$Context = New-Object Microsoft.SharePoint.Client.ClientContext($targetURL)
$secpassword = ConvertTo-SecureString $o365password -AsPlainText -Force
$Credentials = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($o365username,$secpassword)
$Context.RequestTimeout = 16384000
$Context.Credentials = $Credentials

try{
    $web = $Context.Web 
    $Context.Load($web)
    #connect to sharepoint
    if($libraryName){
        $library = $web.Lists.GetByTitle($libraryName)
    #or to Onedrive
    }else{
        $lists = $web.Lists
        $Context.Load($lists)
        $Context.ExecuteQuery()
        foreach($list in $lists){
            if($list.BaseType -eq "DocumentLibrary" -and $list.EntityTypeName -eq "Documents"){
                $library = $list
                break;
            }
        }
        if(!$library){
            Throw "Connected to the site, but could not detect the default document library! This is likely a permissions issue"
        }
    }
    $Context.Load($library)
    $Context.Load($library.RootFolder)
    $Context.Load($library.Fields)
    $Context.ExecuteQuery()
    $resetVersioning = $False
    $enableVersioning = $library.EnableVersioning
    if ($enableVersioning -eq $true){
        $script:adminLogBuffer += "$sourceFolder | VERBOSE | disabled versioning on this library`r`n" 
        Write-Host "$sourceFolder : disabled versioning on this library" -ForegroundColor Green    
        $resetVersioning = $True
	    $library.EnableVersioning = $false
	    $library.Update()
	    $Context.ExecuteQuery()
    }
    $resetCheckout = $False
    $forceCheckout = $library.ForceCheckout
    if ($forceCheckout -eq $true){
        $script:adminLogBuffer += "$sourceFolder | VERBOSE | disabled forced checkout on this library`r`n" 
        Write-Host "$sourceFolder : disabled forced checkout on this library" -ForegroundColor Green   
        $resetCheckout = $True
	    $library.ForceCheckout = $false
	    $library.Update()
	    $Context.ExecuteQuery()
    }
}catch{
    if($libraryName){
        $adminLogBuffer += "$sourceFolder | CRITICAL ERROR | Could not connect to $targetURL | $($Error[0])`r`n"  
        Write-Host "$sourceFolder : Could not connect to $targetURL  $($Error[0])" -ForegroundColor Red
    }else{
        $adminLogBuffer += "$sourceFolder | CRITICAL ERROR | Could not connect to user's Onedrive URL: $targetURL, library $($defaultLibraryObject.Title), has it been provisioned? | $($Error[0])`r`n"  
        Write-Host "$sourceFolder : Could not connect to user's Onedrive URL, has it been provisioned?  $($Error[0])" -ForegroundColor Red
    }
    abort -failed -writeAdminLog
}

#if we're uploading homedirs, check if the supplied username exists in Sharepoint, so we can set him/her as author/editor
if($libraryName){
    $userLookupString = $Null
}else{
    try{
        $user = $context.Web.EnsureUser($username)
        $context.Load($user)
        $context.ExecuteQuery()
        $userLookupString = "{0};#{1}" -f $user.Id, $user.LoginName
        $adminLogBuffer += "$sourceFolder | VERBOSE | found $username, author and editor properties will be set`r`n"  
        Write-Host "$sourceFolder : found $username, author and editor properties will be set" -ForegroundColor Green
    }catch{
        $userLookupString = $Null
        $adminLogBuffer += "$sourceFolder | WARNING | could not find $username, author and editor properties will NOT be set`r`n"  
        Write-Host "$sourceFolder : could not find $username, author and editor properties will NOT be set" -ForegroundColor Red
    }    
}

function doFolder{
    Param(
        [Parameter(Mandatory=$True)][String]$path,
        [Parameter(Mandatory=$True)][Microsoft.SharePoint.Client.Web]$web,
        [Parameter(Mandatory=$True)][Microsoft.SharePoint.Client.Folder]$listFolder
    )

    if($specificFolder -and $specificFolder -ne "CMD_EXCEPTION_FILES_ONLY"){
        $topLevel = Join-Path $sourceFolder -ChildPath $specificFolder
    }else{
        $topLevel = $Null
    }

    Get-ChildItem -LiteralPath $path | % {        
        #If it is a folder
        if ($_.PSIsContainer -eq $True){
            if(($specificFolder -eq $Null -or $_.FullName.StartsWith($topLevel))) { 
                if($_.Name -and $folderIgnoreArray -contains $_.Name){
                    $script:adminLogBuffer += "$sourceFolder | VERBOSE | skipping $($_.Name) folder because it is in the folderIgnoreList |`r`n" 
                    Write-Host "$sourceFolder : skipping $($_.Name) folder because it is in the folderIgnoreList" -ForegroundColor Green                     
                }elseif($_.Name){
                    try{
                        $subFolder = Ensure-Folder -Web $web -ParentFolder $listFolder -newFolder $_
                        doFolder -path $_.FullName -web $web -listFolder $subFolder
                    }catch{
                        $Null
                    }
                }
            }
        #If it is a file
        }else{
            if(($specificFolder -eq "CMD_EXCEPTION_FILES_ONLY" -and $path -ne $sourceFolder) -or ($path -eq $sourceFolder -and $specificFolder -ne "CMD_EXCEPTION_FILES_ONLY" -and $specificFolder)){
                #do nothing
            }else{
                $fileUrl = "$($listFolder.ServerRelativeUrl)/$($_.Name)"
                try{
                    Upload-File -Web $web -fileUrl $fileUrl -LocalFile $_
                }catch{
                    $Null
                }
            }
        }
    }
}

#if a target folder was specified, create it and use this as root
$targetLibraryFolderPath = $library.RootFolder
if($targetFolder){
    $adminLogBuffer += "$sourceFolder | VERBOSE | $targetFolder specified as target subfolder |`r`n"  
    Write-Host "$sourceFolder : $targetFolder specified as target subfolder" -ForegroundColor Green        
    try{
        $targetLibraryFolderPath = Ensure-Folder -Web $web -ParentFolder $targetLibraryFolderPath -newFolder $targetFolder -root
        doFolder -path $sourceFolder -web $web -listFolder $targetLibraryFolderPath
        abort -writeAdminLog
    }catch{
        $adminLogBuffer += "$sourceFolder | CRITICAL ERROR | Cannot continue because $targetFolder could not be created |`r`n"  
        Write-Host "$sourceFolder : Cannot continue because $targetFolder could not be created" -ForegroundColor Red
        abort -writeAdminLog -failed
    }
}else{
    try{
        doFolder -path $sourceFolder -web $web -listFolder $targetLibraryFolderPath
        abort -writeAdminLog
    }catch{
        $adminLogBuffer += "$sourceFolder | CRITICAL ERROR | Initialization of doFolder function failed: $($Error[0]) |`r`n"  
        Write-Host "$sourceFolder : Initialization of doFolder function failed: $($Error[0])" -ForegroundColor Red
        abort -writeAdminLog -failed
    }
}
